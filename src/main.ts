import { NestFactory } from '@nestjs/core';
import { ApplicationModule } from './app/app.module';
import { createCorsOptions } from './app/common/cors';
import { environment } from './environments/environment';
import { grpcClientOptions } from './app/grpc-client-options';
import { ClientOptions } from '@nestjs/microservices';

import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

process.on('unhandledRejection', (r) => console.error(r));

async function bootstrap() {
  const app = await NestFactory.create(ApplicationModule);

  const config = environment; // FIXME

  app.connectMicroservice({
    ...grpcClientOptions,
    options: {
      ...grpcClientOptions.options,
      url: `${config.grpc.host}:${config.grpc.port}`,
    },
  } as ClientOptions);

  await app.startAllMicroservices();

  app.enableCors(createCorsOptions(config.whitelist));

  const document = SwaggerModule.createDocument(
    app,
    new DocumentBuilder()
      .setTitle('Token Service API')
      .setDescription('')
      .setVersion('1.0')
      .addTag('token')
      .build(),
  );
  SwaggerModule.setup('api', app, document);

  await app.listen(config.port);

  console.info(`@authumn/user is listening at port ${config.port}`);
  console.info(
    `@authumn/user grpc server is listening at ${config.grpc.host}:${config.grpc.port}`,
  );
}

(async () => {
  await bootstrap();
})();
