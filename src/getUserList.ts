// TODO: use @grpc
// var grpc = require('@grpc/grpc-js');
// var protoLoader = require('@grpc/proto-loader');
import * as caller from 'grpc-caller';
import { resolve } from 'path';

const PROTO_PATH = resolve(__dirname, './app/assets/user.proto');

const userService = caller('0.0.0.0:50051', PROTO_PATH, 'UserService');

(async () => {
  const { users } = await userService.Preload({});

  console.log(users);

  const res = await userService.GetUsername({ id: users[0].id });

  console.log(res);
})();
