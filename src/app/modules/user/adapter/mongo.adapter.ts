import { IUserDbAdapter } from './IUserDbAdapter';
import { Injectable, Inject } from '@nestjs/common';
import { User } from '../models';
import { ConfigService } from '@nestling/config';
import * as uuid from 'uuid';
import { Db, DeleteResult, Filter, FindOptions, MongoClient } from 'mongodb';

@Injectable()
export class MongoDbAdapter implements IUserDbAdapter {
  private readonly db: Db;
  constructor(
    @Inject('MongoDbToken') readonly mongo: MongoClient,
    private config: ConfigService,
  ) {
    this.db = mongo.db((this.config as any).mongo.database);
  }
  // find<T = TSchema>(query?: FilterQuery<TSchema>): Cursor<T>;
  async find(
    query: Filter<User> = {},
    options?: FindOptions<User>,
  ): Promise<User[]> {
    return this.db.collection<User>('users').find(query, options).toArray();
  }

  async findOne(by: Filter<User>): Promise<User> {
    return this.db.collection<User>('users').findOne(by);
  }

  async insert(user: User): Promise<User> {
    const collection = this.db.collection<User>('users');

    const id = uuid.v4();

    const result = await collection.insertOne({
      ...user,
      id,
      created_at: new Date(),
      updated_at: new Date(),
    });

    const updated = await this.findById(id);

    return {
      id,
      email: updated.email,
      username: updated.username,
    };
  }

  async findById(id: string): Promise<User> {
    const collection = this.db.collection<User>('users');

    return collection.findOne({ id });
  }

  async update(user: Partial<User>): Promise<User> {
    if (!user.id) {
      throw Error('Update requires user.id to be set');
    }
    const _user = await this.findById(user.id);

    const collection = this.db.collection<User>('users');

    const changes = {};

    Object.keys(user).forEach((key) => {
      if (_user[key] !== user[key] && key !== '_id' && key !== 'id') {
        changes[key] = user[key];
      }
    });

    const result = await collection.updateOne(
      { id: user.id },
      {
        $currentDate: {
          updated_at: true,
        },
        $set: changes,
      },
    );

    if (result && result.modifiedCount === 1) {
      return this.findById(user.id);
    }

    throw Error('Failed to update user.');
  }

  async findAll(): Promise<User[]> {
    return this.find({});
  }

  async flush(): Promise<DeleteResult> {
    return this.db.collection<User>('users').deleteMany({});
  }
}
