import { User } from '../models';
import { Filter, FindOptions } from 'mongodb';

export interface IUserDbAdapter {
  // find<T = any>(query: Filter<any>, options?: any): Promise<Cursor<T>>
  find(query: Filter<User>, options?: FindOptions): Promise<User[]>;
  findOne: (by: Filter<User>) => Promise<User>;
  insert: (user: User) => Promise<User>;
  findById: (id: string) => Promise<User>;
  update: (user: User) => Promise<User>;
  findAll: () => Promise<User[]>;
}
