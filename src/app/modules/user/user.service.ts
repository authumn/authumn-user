import { Injectable } from '@nestjs/common';
import { User, UserMap } from './models';
import { MongoDbAdapter } from './adapter/mongo.adapter';
import * as bcrypt from 'bcrypt';
import { PasswordService } from './password.service';
import { ConfigService } from '@nestling/config';
import { ErrorMessage } from '@nestling/errors';
import { IUserService } from './interfaces/IUserService';

@Injectable()
export class UserService implements IUserService {
  private _user!: User;

  constructor(
    private config: ConfigService,
    private adapter: MongoDbAdapter,
    private passwordService: PasswordService,
  ) {}

  set user(user: User) {
    this._user = user;
  }

  get user(): User {
    return this._user;
  }

  async updatePassword(newPassword: string): Promise<User> {
    const password = await this.passwordService.hash(newPassword);

    return this.adapter.update({
      id: this.user.id as string,
      password,
    });
  }

  /**
   * Authenticate user by providing email and password.
   *
   * @param {string} login Email or username
   * @param {string} password
   * @returns {Promise<User>}
   */
  async authenticate(login: string, password: string): Promise<User> {
    let user = await this.findByEmail(login);

    if (!user) {
      user = await this.findByUsername(login);
    }

    if (user) {
      const match = await bcrypt.compare(password, user.password as string);

      if (match) {
        return {
          id: user.id as string,
          email: user.email,
          username: user.username,
        };
      }
    }

    throw new ErrorMessage('user:credentialsInvalid');
  }

  /**
   * Find a user by id and email
   *
   * @param {any} where
   * @returns {Promise<User | undefined>}
   */
  async findOne(where: any): Promise<User | null> {
    return this.adapter.findOne(where);
  }

  /**
   * Find a user by id
   *
   * @param {string} id
   * @returns {Promise<User | undefined>}
   */
  async findById(id: string): Promise<User | null> {
    return this.adapter.findById(id);
  }

  /**
   * Find a user by email
   *
   * @param {string} email
   * @returns {Promise<User>}
   */
  async findByEmail(email: string): Promise<User | null> {
    return this.adapter.findOne({
      email,
    });
  }

  /**
   * Find a user by username
   *
   * @param {string} username
   * @returns {Promise<User>}
   */
  async findByUsername(username: string): Promise<User | null> {
    return this.adapter.findOne({
      username,
    });
  }

  /**
   * List all users
   *
   * @returns {Promise<User[]>}
   */
  async listUsers(): Promise<User[]> {
    return this.adapter.find({});
  }

  async idAndNamelist(): Promise<UserMap[]> {
    const result = await this.adapter.find({}, { id: 1, username: 1 } as any);

    return result.map((user) => ({
      id: user.id,
      name: user.username,
    }));
  }

  /**
   * Update a user.
   *
   * @param {User} user
   * @returns {Promise<User>}
   */
  async update(user: User): Promise<User> {
    return this.adapter.update(user);
  }

  /**
   * Register a user.
   *
   * @param {string} email
   * @param {string} password
   * @returns {Promise<User>}
   */
  async register(
    email: string,
    password: string,
    additionalInfo: any = {},
  ): Promise<User> {
    return this.createUser(email, password, additionalInfo);
  }

  async createUser(
    email: string,
    password: string,
    additionalInfo: any = {},
  ): Promise<User> {
    const hashedPassword = await bcrypt.hash(
      password,
      (this.config as any).saltRounds,
    );

    const user = {
      email,
      password: hashedPassword,
      ...additionalInfo,
    };

    return this.adapter.insert(user);
  }
}
