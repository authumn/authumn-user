export interface User {
  _id?: string;
  id?: string;
  email: string;
  username?: string;
  password?: string;
  firstName?: string;
  lastName?: string;
  avatar_url?: string;
  created_at?: Date;
  updated_at?: Date;
}
