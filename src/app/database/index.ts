import { Module } from '@nestjs/common';
import { mongoProvider } from '@nestling/mongodb';
import { redisProvider } from '@nestling/redis';

@Module({
  exports: [mongoProvider, redisProvider],
  providers: [mongoProvider, redisProvider],
})
export class DatabaseModule {}
