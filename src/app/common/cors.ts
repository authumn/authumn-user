export function createCorsOptions(whitelist: string[]) {
  function origin(orig: string | undefined, callback: any) {
    if (orig === undefined || whitelist.indexOf(orig) !== -1) {
      callback(null, true);
    } else {
      callback(new Error(`not allowed by cors: ${orig}`));
    }
  }

  return {
    origin,
  };
}
